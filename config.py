import logging
import os

BACKEND = 'Mattermost'  # Errbot will start in text mode (console only mode) and will answer commands from there.

ROOT = os.environ.get('COBOT_ROOT', os.getcwd())

BOT_DATA_DIR = r'/home/lasse/prog/gitmate/mattbot/data'
BOT_EXTRA_PLUGIN_DIR = r'/home/lasse/prog/gitmate/mattbot/plugins'

BOT_LOG_FILE = r'/home/lasse/prog/gitmate/mattbot/errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

BOT_ADMINS = ('@sils', )  # !! Don't leave that to "@CHANGE_ME" if you connect your errbot to a chat system !!

BOT_IDENTITY = {
    # Required
    'team': 'GitMate',
    'server': 'mattermost.gitmate.io',
    # For the login, either
    'login': 'bot@email.de',
    'password': 'botpassword',
    # Or, if you have a personal access token
    'token': 'YourPersonalAccessToken',
    # Optional
    'insecure': False, # Default = False. Set to true for self signed certificates
    'scheme': 'https', # Default = https
    'port': 8065, # Default = 8065
    'timeout': 30, # Default = 30. If the webserver disconnects idle connections later/earlier change this value
    'cards_hook': 'incomingWebhookId' # Needed for cards/attachments
}
